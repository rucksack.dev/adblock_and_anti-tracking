/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package g11n

import core.Kontext
import core.Result
import org.junit.Assert
import org.junit.Test

class TranslationsFetcherTest {
    @Test fun fetcher_basics() {
        var put = false
        val fetcher = TranslationsFetcher(
                urls = { mapOf("http://localhost" to "fixture") },
                doFetchTranslations = { url, prefix ->
                    Result.of { listOf("fixture_key1" to "translation1") }
                },
                doPutTranslation = { key, translation ->
                    put = true
                    Assert.assertEquals("fixture_key1", key)
                    Assert.assertEquals("translation1", translation)
                    Result.of { true }
                }
        )

        fetcher.sync(Kontext.forTest())
        Assert.assertTrue(put)
    }
}
