/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.get
import com.github.michaelbull.result.or
import core.bits.SlotStatusPersistence
import io.paperdb.Paper

class Persistence {
    companion object {
        const val DEFAULT_PATH = ""
        val global = GlobalPersistence()
        val slots = SlotStatusPersistence()

        fun paper() = {
            with(Persistence.global.loadPath()) {
                if (this != DEFAULT_PATH) Paper.bookOn(this)
                else Paper.book()
            }
        }()
    }
}

class GlobalPersistence {
    internal val loadPath = {
        Result.of { Paper.book().read<String>("persistencePath", Persistence.DEFAULT_PATH) }
                .or { Ok(Persistence.DEFAULT_PATH) }.get()
    }

    val savePath = { path: String ->
        Result.of { Paper.book().write("persistencePath", path) }
    }

    fun getPathForSmartList() = {
        val path = loadPath()
        if (path.isNullOrBlank()) {
            val newPath = getActiveContext()?.filesDir?.absolutePath
            if (newPath != null) "$newPath/"
            else {
                e("could not load path for SmartList")
                ""
            }
        } else path
    }()

    fun isDefaultLoadPath() = loadPath() == Persistence.DEFAULT_PATH
}
