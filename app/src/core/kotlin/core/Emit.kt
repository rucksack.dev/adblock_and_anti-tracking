/*
Copyright 2020 blokada of https://blokada.org represented by 
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import kotlinx.coroutines.*

private val commonEmit = CommonEmit()

class EventType<T>(val name: String) {
    override fun toString() = name
}

typealias SimpleEvent = EventType<Unit>
typealias Callback<T> = (T) -> Unit
private data class TypedEvent<T>(val type: EventType<T>, val value: T)

fun String.newEvent() = SimpleEvent(this)
fun <T> String.newEventOf() = EventType<T>(this)

interface Emit {
    fun <T> emit(event: EventType<T>, value: T): Job
    fun <T> on(event: EventType<T>, callback: Callback<T>): Job
    fun <T> on(event: EventType<T>, callback: Callback<T>, recentValue: Boolean = true): Job
    fun <T> cancel(event: EventType<T>, callback: Callback<T>): Job
    suspend fun <T> getMostRecent(event: EventType<T>): T?
    fun emit(event: SimpleEvent): Job
    fun on(event: SimpleEvent, callback: () -> Unit, recentValue: Boolean = true): Job
    fun cancel(event: SimpleEvent, callback: () -> Unit): Job
}


class CommonEmit(
        private val ktx: () -> Kontext = { Kontext.forCoroutine(Dispatchers.Main + newEmitExceptionLogger(), "emit") }
) : Emit {

    private val emits = mutableMapOf<EventType<*>, TypedEvent<*>>()
    private val callbacks = mutableMapOf<EventType<*>, MutableList<Callback<*>>>()
    private val simpleEmits = mutableSetOf<EventType<Unit>>()
    private val simpleCallbacks = mutableMapOf<EventType<Unit>, MutableList<() -> Unit>>()

    override fun <T> emit(event: EventType<T>, value: T) = GlobalScope.launch(ktx().coroutineContext()) {
        val e = TypedEvent(event, value)
        emits[event] = e

        if (callbacks.containsKey(event)) for (callback in callbacks[event]!!)
            (callback as Callback<T>)(e.value)
    }

    override fun <T> on(event: EventType<T>, callback: Callback<T>) = on(event, callback, recentValue = true)

    override fun <T> on(event: EventType<T>, callback: Callback<T>, recentValue: Boolean) = GlobalScope.launch(ktx().coroutineContext()) {
        callbacks.getOrPut(event, { mutableListOf() }).add(callback as Callback<*>)
        if (recentValue) emits[event]?.apply { callback(this.value as T) }
    }

    override fun <T> cancel(event: EventType<T>, callback: Callback<T>) = GlobalScope.launch(ktx().coroutineContext()) {
        callbacks[event]?.remove(callback)
    }

    override fun emit(event: SimpleEvent) = GlobalScope.launch(ktx().coroutineContext()) {
        simpleEmits.add(event)

        if (simpleCallbacks.containsKey(event)) for (callback in simpleCallbacks[event]!!)
            callback()
    }

    override fun on(event: SimpleEvent, callback: () -> Unit, recentValue: Boolean) = GlobalScope.launch(ktx().coroutineContext()) {
        simpleCallbacks.getOrPut(event, { mutableListOf() }).add(callback)
        if (recentValue) emits[event]?.apply { callback() }
    }

    override fun cancel(event: SimpleEvent, callback: () -> Unit) =GlobalScope.launch(ktx().coroutineContext()) {
        simpleCallbacks[event]?.remove(callback)
    }

    override suspend fun <T> getMostRecent(event: EventType<T>) = GlobalScope.async(ktx().coroutineContext()) {
        emits[event]
    }.await()?.value as T?

}

internal fun newEmitExceptionLogger(ktx: Kontext = "emit:exception".ktx())
        = CoroutineExceptionHandler { _, throwable -> ktx.e(throwable)
}

class DefaultEmit(id: String, val common: Emit = commonEmit, val log: Log = DefaultLog(id)) : Emit {

    override fun <T> emit(event: EventType<T>, value: T): Job {
        log.v("event:emit", event, value.toString())
        return common.emit(event, value)
    }

    override fun <T> on(event: EventType<T>, callback: Callback<T>) = on(event, callback, true)

    override fun <T> on(event: EventType<T>, callback: Callback<T>, recentValue: Boolean): Job {
        //log.v("event:subscriber:on", event, callback)
        return common.on(event, callback, recentValue)
    }

    override fun <T> cancel(event: EventType<T>, callback: Callback<T>): Job {
        //log.v("event:subscriber:cancel", event, callback)
        return common.cancel(event, callback)
    }

    override suspend fun <T> getMostRecent(event: EventType<T>) = common.getMostRecent(event)

    override fun emit(event: SimpleEvent): Job {
        log.v("event emit", event)
        return common.emit(event)
    }

    override fun on(event: SimpleEvent, callback: () -> Unit, recentValue: Boolean): Job {
        //log.v("event:subscriber:on", event, callback)
        return common.on(event, callback)
    }

    override fun cancel(event: SimpleEvent, callback: () -> Unit): Job {
        //log.v("event:subscriber:cancel", event, callback)
        return common.cancel(event, callback)
    }
}

fun <T> emit(event: EventType<T>, value: T) = commonEmit.emit(event, value)
fun <T> on(event: EventType<T>, callback: Callback<T>) = on(event, callback, true)
fun <T> on(event: EventType<T>, callback: Callback<T>, recentValue: Boolean) = commonEmit.on(event, callback, recentValue)
fun <T> cancel(event: EventType<T>, callback: Callback<T>) = commonEmit.cancel(event, callback)
suspend fun <T> getMostRecent(event: EventType<T>) = commonEmit.getMostRecent(event)
fun emit(event: SimpleEvent) = commonEmit.emit(event)
fun on(event: SimpleEvent, callback: () -> Unit, recentValue: Boolean) = commonEmit.on(event, callback, recentValue)
fun cancel(event: SimpleEvent, callback: () -> Unit) = commonEmit.cancel(event, callback)
