/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat.checkSelfPermission
import kotlinx.coroutines.CompletableDeferred

private var deferred = CompletableDeferred<Boolean>()

private const val REQUEST_STORAGE = 2

fun askStoragePermission(ktx: Kontext, act: Activity) = {
    ktx.v("asking for storage permissions")
    if (Build.VERSION.SDK_INT >= 23) {
        deferred.completeExceptionally(Exception("new permission request"))
        deferred = CompletableDeferred()
        if (checkSelfPermission(act, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            deferred.complete(true)
        } else act.requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_STORAGE)
    } else deferred.complete(true)
    deferred
}()

fun storagePermissionResult(ktx: Kontext, code: Int) = {
    ktx.v("received storage permissions response", code)
    when {
        deferred.isCompleted -> Unit
        code == PackageManager.PERMISSION_GRANTED -> deferred.complete(true)
        else -> deferred.completeExceptionally(Exception("permission result: $code"))
    }
}()

fun checkStoragePermissions(ktx: AndroidKontext) = {
    if (Build.VERSION.SDK_INT >= 23) checkSelfPermission(ktx.ctx,
            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    else true
}()
