/*
Copyright 2020 blokada of https://blokada.org represented by 
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package adblocker

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import com.github.salomonbrys.kodein.instance
import core.Tunnel
import gs.environment.inject
import notification.ANotificationsToggleService
import org.blokada.R
import android.widget.RemoteViews
import notification.NotificationsToggleSeviceSettings
import tunnel.RequestLog
import tunnel.RequestState


class ListWidgetProvider : AppWidgetProvider() {

    override fun onUpdate(context: Context?, appWidgetManager: AppWidgetManager?, appWidgetIds: IntArray?) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        val remoteViews = RemoteViews(context!!.packageName,
                R.layout.view_list_widget)
        val t: Tunnel = context.inject().instance()

        var domainList = ""
        RequestLog
                .getRecentHistory()
                .filter { it.state == RequestState.BLOCKED_NORMAL }
                .take(50)
                .asReversed()
                .distinct()
                .forEach { request ->
                    domainList += request.domain + '\n'
                }
        remoteViews.setTextViewText(R.id.widget_list_message, domainList)

        val intent = Intent(context, ANotificationsToggleService::class.java)
        intent.putExtra("new_state", !t.enabled())
        intent.putExtra("setting", NotificationsToggleSeviceSettings.GENERAL)
        remoteViews.setOnClickPendingIntent(R.id.widget_list_button, PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT))
        if (t.enabled()) {
            remoteViews.setInt(R.id.widget_list_icon, "setColorFilter", color(context, active = true, waiting = false))
            remoteViews.setTextViewText(R.id.widget_list_button, context.resources.getString(R.string.notification_keepalive_deactivate))
        } else {
            remoteViews.setInt(R.id.widget_list_icon, "setColorFilter", color(context, active = false, waiting = false))
            remoteViews.setTextViewText(R.id.widget_list_button, context.resources.getString(R.string.notification_keepalive_activate))
        }
        appWidgetManager?.updateAppWidget(appWidgetIds, remoteViews)
    }

    private fun color(ctx: Context, active: Boolean, waiting: Boolean): Int {
        return when {
            waiting -> ctx.resources.getColor(R.color.colorLogoWaiting)
            active -> ctx.resources.getColor(android.R.color.transparent)
            else -> ctx.resources.getColor(R.color.colorLogoInactive)
        }
    }
}
