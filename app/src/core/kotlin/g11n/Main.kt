/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package g11n

import core.COMMON
import core.Kontext
import core.Result
import core.Url
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

object Events {

}

class Main(
        urls: () -> Map<Url, Prefix>,
        doPutTranslation: (Key, Translation) -> Result<Boolean>
) {

    private val fetcher = TranslationsFetcher(urls, doPutTranslation = doPutTranslation)

    fun load(ktx: Kontext) = GlobalScope.async(COMMON) {
        fetcher.load(ktx)
    }

    fun sync(ktx: Kontext) = GlobalScope.async(COMMON) {
        fetcher.sync(ktx)
        fetcher.save(ktx)
    }

    fun invalidateCache(ktx: Kontext) = GlobalScope.async(COMMON) {
        fetcher.invalidateCache(ktx)
    }
}
