package ads;

import android.content.Context;

import org.blokada.BuildConfig;

public abstract class Ad<T> {

    private String adUnitId;
    private Context mContext;

    Ad(Context context) {
        this.mContext = context;
    }

    public void setAdUnitId(String adUnitId) {
        this.adUnitId = adUnitId;
    }

    <T> String getAdUnitId(Ad<T> ad) {
        return getAdMobAdUnitId(ad.getClass());
    }

    private String getAdMobAdUnitId(Class ad) {
        if(ad.equals(MoPubView.class))
            return BuildConfig.DEBUG ? "b195f8dd8ded45fe847ad89ed1d016da" : "3139cb02dfd7445f9dd21ab753bf76de";
        else if(ad.equals(MoPubInterstitial.class))
            return BuildConfig.DEBUG ? "24534e1901884e398f1253216226017e" : "4bc7727c62a24012ac0c83c8cbbf538e";
        else {
            throw new NullPointerException("Kann AdUnitID nicht feststellen.");
        }
    }

    public abstract void destroy();

    public abstract void load();

    @Override
    public String toString(){
        return "AdUnitId: " + adUnitId + ".";
    }

}
