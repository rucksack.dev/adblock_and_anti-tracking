package ads;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;
import com.mopub.common.logging.MoPubLog;
import com.mopub.common.privacy.ConsentDialogListener;
import com.mopub.common.privacy.ConsentStatus;
import com.mopub.common.privacy.ConsentStatusChangeListener;
import com.mopub.common.privacy.PersonalInfoManager;
import com.mopub.mobileads.FacebookAdapterConfiguration;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.SmaatoAdapterConfiguration;
import com.smaato.sdk.core.SmaatoSdk;
import com.smaato.sdk.core.log.LogLevel;

import org.blokada.BuildConfig;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sönke Gissel on 16.12.2019.
 */
public class MoPubSdk {

    @SuppressLint("StaticFieldLeak")
    private static MoPubSdk INSTANCE;
    private final MutableLiveData<Boolean> isMoPubSdkInitialized = new MutableLiveData<>();
    private Activity mActivity;

    private PersonalInfoManager mPersonalInfoManager;
    private JSONObject consentDict;
    private MoPubInitListener listener;

    public void setMoPubInitListener(MoPubInitListener listener) {
        this.listener = listener;
    }

    private MoPubSdk(Activity activity) {
        this.mActivity = activity;

        Map<String, String> facebookSettings = new HashMap<>();
        facebookSettings.put("placement_ids", "2920919844795914_2920921618129070");

        Map<String, String> smaatoAdapterConfiguration = new HashMap<>();
        smaatoAdapterConfiguration.put(SmaatoAdapterConfiguration.KEY_ENABLE_LOGGING, String.valueOf(true));
        //smaatoAdapterConfiguration.put(SmaatoAdapterConfiguration.KEY_HTTPS_ONLY, String.valueOf(true));
        smaatoAdapterConfiguration.put(SmaatoAdapterConfiguration.KEY_LOG_LEVEL, String.valueOf(LogLevel.DEBUG));
        //smaatoAdapterConfiguration.put(SmaatoAdapterConfiguration.KEY_MAX_AD_CONTENT_RATING, String.valueOf(AdContentRating.MAX_AD_CONTENT_RATING_UNDEFINED));
        smaatoAdapterConfiguration.put(SmaatoAdapterConfiguration.KEY_PUBLISHER_ID, "1100044936");

        SdkConfiguration sdkConfiguration = new SdkConfiguration.Builder("3139cb02dfd7445f9dd21ab753bf76de")
                .withLogLevel(BuildConfig.DEBUG ? MoPubLog.LogLevel.DEBUG : MoPubLog.LogLevel.DEBUG)
                .withMediatedNetworkConfiguration(FacebookAdapterConfiguration.class.getName(), facebookSettings)
                .withAdditionalNetwork(SmaatoAdapterConfiguration.class.getName())
                .withMediatedNetworkConfiguration(SmaatoAdapterConfiguration.class.getName(), smaatoAdapterConfiguration)
                .build();
        MoPub.initializeSdk(activity, sdkConfiguration, initSdkListener());

        mPersonalInfoManager = MoPub.getPersonalInformationManager();
        if (mPersonalInfoManager != null) {
            mPersonalInfoManager.subscribeConsentStatusChangeListener(initConsentChangeListener());
        }

        //Nur als InMobi Test
        // InMobiSdk.init(mActivity, "2f401a3fe0034a399e0f88c44fab1c66", InMobiGDPR.getGDPRConsentDictionary());
    }

    public static MoPubSdk getInstance(Activity activity) {
        if(MoPubSdk.INSTANCE == null) {
            MoPubSdk.INSTANCE = new MoPubSdk(activity);
        }
        return MoPubSdk.INSTANCE;
    }

    private SdkInitializationListener initSdkListener() {
        return new SdkInitializationListener() {
            @Override
            public void onInitializationFinished() {
           /* MoPub SDK initialized.
           Check if you should show the consent dialog here, and make your ad requests. */
                Log.d("MoPub", "SDK initialized");
                isMoPubSdkInitialized.setValue(true);
                listener.onInitialized();
                if (mPersonalInfoManager != null && mPersonalInfoManager.shouldShowConsentDialog()) {
                    mPersonalInfoManager.loadConsentDialog(initDialogLoadListener());
                }

                SmaatoSdk.setGPSEnabled(true);
            }
        };
    }

    public LiveData<Boolean> isMoPubSdkInitialized() {
        return isMoPubSdkInitialized;
    }

    private ConsentStatusChangeListener initConsentChangeListener() {
        return new ConsentStatusChangeListener() {
            @Override
            public void onConsentStateChange(@NonNull ConsentStatus oldConsentStatus,
                                             @NonNull ConsentStatus newConsentStatus, boolean canCollectPersonalInformation) {
                Log.d(this.getClass().getName(), "Consent: " + newConsentStatus.name());
                if (mPersonalInfoManager != null && mPersonalInfoManager.shouldShowConsentDialog()) {
                    mPersonalInfoManager.loadConsentDialog(initDialogLoadListener());
                }
                if (newConsentStatus.equals(ConsentStatus.EXPLICIT_YES)) {
                    //setInMobiGDPR(true);
                    setSmaatoGDPR(true);
                } else if (newConsentStatus.equals(ConsentStatus.EXPLICIT_NO)) {
                    //setInMobiGDPR(false);
                    setSmaatoGDPR(false);
                }
            }
        };
    }

    private void setSmaatoGDPR(boolean userConsent) {
        // User is not subject to GDPR
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("IABConsent_SubjectToGDPR", userConsent ? "0" : "1");
        editor.apply();
    }

/*    private void setInMobiGDPR(boolean userConsent) {
        consentDict = new JSONObject();
        try {
            consentDict.put(InMobiSdk.IM_GDPR_CONSENT_AVAILABLE, userConsent);
            consentDict.put("gdpr", userConsent ? 1 : 0);
            //Use InMobiSdk.IM_GDPR_CONSENT_IAB constant to pass the gdpr consent in IAB format
        } catch (JSONException e) {
            MoPubLog.log(MoPubLog.AdapterLogEvent.CUSTOM, this.getClass().getName(), "Unable to set GDPR consent object");
            MoPubLog.log(MoPubLog.SdkLogEvent.ERROR, this.getClass().getName(), e.getMessage());
        }
        InMobiGDPR.setGDPRConsentDictionary(consentDict);
    }*/

    private ConsentDialogListener initDialogLoadListener() {
        return new ConsentDialogListener() {

            @Override
            public void onConsentDialogLoaded() {
                if (mPersonalInfoManager != null) {
                    mPersonalInfoManager.showConsentDialog();
                }
            }

            @Override
            public void onConsentDialogLoadFailed(@NonNull MoPubErrorCode moPubErrorCode) {
                Log.d(this.getClass().getName(), "Consent dialog failed to load.");
            }
        };
    }
}
