/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package blocka

import java.util.*

internal class BlockaVpnManager(
        internal var enabled: Boolean,
        private val accountManager: AccountManager,
        private val leaseManager: LeaseManager,
        private val scheduleAccountCheck: () -> Any
) {

    fun restoreAccount(newId: AccountId) {
        accountManager.restoreAccount(newId)
    }

    fun sync(force: Boolean = false) {
        try {
            accountManager.sync(force)
            if (!enabled) return
            leaseManager.sync(accountManager.state)
            //enabled = enabled && (accountManager.state.accountOk && leaseManager.state.leaseOk)
            val isOk = accountManager.state.accountOk && leaseManager.state.leaseOk
            if (isOk) scheduleAccountCheck()
        } catch (ex: Exception) {
            //enabled = false
            when {
                ex is BoringTunLoadException -> throw ex
                accountManager.state.activeUntil.expired() && enabled -> throw BlockaAccountExpired()
                ex is BlockaRestModel.TooManyDevicesException -> throw BlockaTooManyDevices()
                ex is BlockaGatewayNotSelected -> throw ex
                accountManager.state.id.isBlank() -> throw BlockaAccountEmpty()
                !accountManager.state.accountOk -> throw BlockaAccountNotOk()
                !leaseManager.state.leaseOk -> throw BlockaLeaseNotOk()
            }

            throw Exception("failed syncing blocka vpn", ex)
        }
    }

    fun shouldSync() = accountManager.state.id.isEmpty() || leaseManager.state.leaseActiveUntil.before(Date())
}

class BlockaAccountExpired: Exception()
class BlockaAccountEmpty: Exception()
class BlockaAccountNotOk: Exception()
class BlockaLeaseNotOk: Exception()
class BlockaGatewayNotSelected: Exception()
class BlockaTooManyDevices: Exception()
