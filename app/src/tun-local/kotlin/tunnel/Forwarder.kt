/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package tunnel

import core.Result
import core.Time
import core.w
import org.pcap4j.packet.Packet
import java.net.DatagramSocket
import java.util.*

internal class Forwarder(val ttl: Time = 10 * 1000) : Iterable<ForwardRule> {

    private val store = LinkedList<ForwardRule>()

    fun add(socket: DatagramSocket, originEnvelope: Packet) {
        if (store.size >= 1024) {
            w("forwarder reached 1024 open sockets")
            Result.of { store.element().socket.close() }
            store.remove()
        }
        while (store.isNotEmpty() && store.element().isOld()) {
            Result.of { store.element().socket.close() }
            store.remove()
        }
        store.add(ForwardRule(socket, originEnvelope, ttl))
    }

    override fun iterator() = store.iterator()

    fun size() = store.size
}

internal data class ForwardRule(
        val socket: DatagramSocket,
        val originEnvelope: Packet,
        val ttl: Time
) {
    val added = System.currentTimeMillis()

    fun isOld(): Boolean {
        return (System.currentTimeMillis() - added) > ttl
    }
}
