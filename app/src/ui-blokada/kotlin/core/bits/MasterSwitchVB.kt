/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core.bits

import blocka.BlockaVpnState
import com.github.salomonbrys.kodein.instance
import core.*
import gs.property.I18n
import org.blokada.R
import tunnel.TunnelConfig

class MasterSwitchVB(
        private val ktx: AndroidKontext,
        private val i18n: I18n = ktx.di().instance(),
        private val tunnelEvents: Tunnel = ktx.di().instance(),
        private val tunnelStatus: EnabledStateActor = ktx.di().instance()
) : core.MasterSwitchVB() {

    private var active = false
    private var activating = false

    override fun attach(view: MasterSwitchView) {
        view.switch(tunnelEvents.enabled(), animation = false)
        tunnelStatus.listeners.add(tunnelListener)
        tunnelStatus.update(tunnelEvents)
        update()
    }

    override fun detach(view: MasterSwitchView) {
        tunnelStatus.listeners.remove(tunnelListener)
    }

    private val update = {
        val config = get(TunnelConfig::class.java)
        val blockaVpnEnabled = get(BlockaVpnState::class.java).enabled

        view?.run {
            onSwitch { enable ->
                tunnelEvents.enabled %= enable
            }

            switch(tunnelEvents.enabled())
//            onTap {
//                tunnelEvents.enabled %= !tunnelEvents.enabled()
//            }

            when {
                !tunnelEvents.enabled() -> {
                    state(R.string.home_blokada_inactive.res())
                }
                activating -> {
                    state(R.string.home_please_wait.res())
                }
                !tunnelEvents.active() -> {
                    state(R.string.home_masterswitch_waiting.res())
                }
                !config.adblocking && !blockaVpnEnabled -> {
                    state(R.string.home_dns_only.res())
                }
                !config.adblocking -> {
                    state(R.string.home_vpn_only.res())
                }
                !blockaVpnEnabled -> {
                    state(R.string.home_blokada_active.res())
                }
                else -> {
                    state(R.string.home_blokada_active_protected.res())
                }
           }
        }
        Unit
    }

    private val tunnelListener = object : IEnabledStateActorListener {
        override fun startActivating() {
            activating = true
            active = false
            update()
        }

        override fun finishActivating() {
            activating = false
            active = true
            update()
        }

        override fun startDeactivating() {
            activating = true
            active = false
            update()
        }

        override fun finishDeactivating() {
            activating = false
            active = false
            update()
        }
    }

}
