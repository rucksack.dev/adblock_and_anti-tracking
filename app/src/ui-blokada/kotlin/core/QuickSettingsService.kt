/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.annotation.TargetApi
import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import com.github.salomonbrys.kodein.instance
import gs.environment.inject
import gs.obsolete.Sync
import org.blokada.R

/**
 *
 */
@TargetApi(24)
class QuickSettingsService : TileService(), IEnabledStateActorListener {

    private val s by lazy { inject().instance<Tunnel>() }
    private val enabledStateActor by lazy { inject().instance<EnabledStateActor>() }
    private var waiting = Sync(false)

    override fun onStartListening() {
        updateTile()
        enabledStateActor.listeners.add(this)
    }

    override fun onStopListening() {
        enabledStateActor.listeners.remove(this)
    }

    override fun onTileAdded() {
        updateTile()
    }

    override fun onClick() {
        if (waiting.get()) return
        s.error %= false
        s.enabled %= !s.enabled()
        updateTile()
    }

    private fun updateTile() {
        if (qsTile == null) return
        if (s.enabled()) {
            qsTile.state = Tile.STATE_ACTIVE
            qsTile.label = getString(R.string.main_status_active_recent)
        } else {
            qsTile.state = Tile.STATE_INACTIVE
            qsTile.label = getString(R.string.main_status_disabled)
        }
        qsTile.updateTile()
    }

    override fun startActivating() {
        waiting.set(true)
        qsTile.label = getString(R.string.main_status_activating)
        qsTile.state = Tile.STATE_ACTIVE
        qsTile.updateTile()
    }

    override fun finishActivating() {
        waiting.set(false)
        qsTile.label = getString(R.string.main_status_active_recent)
        qsTile.state = Tile.STATE_ACTIVE
        qsTile.updateTile()
    }

    override fun startDeactivating() {
        waiting.set(true)
        qsTile.label = getString(R.string.main_status_deactivating)
        qsTile.state = Tile.STATE_INACTIVE
        qsTile.updateTile()
    }

    override fun finishDeactivating() {
        waiting.set(false)
        qsTile.label = getString(R.string.main_status_disabled)
        qsTile.state = Tile.STATE_INACTIVE
        qsTile.updateTile()
    }
}
