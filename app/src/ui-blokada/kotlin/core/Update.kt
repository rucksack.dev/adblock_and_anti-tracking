/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.content.Context
import com.github.salomonbrys.kodein.*
import gs.environment.Environment
import gs.environment.Journal
import gs.environment.Time
import gs.environment.Worker
import gs.property.IProperty
import gs.property.Repo
import gs.property.newPersistedProperty
import notification.UpdateNotification
import notification.notificationMain
import org.blokada.BuildConfig
import update.AUpdateDownloader
import update.UpdateCoordinator

abstract class Update {
    abstract val lastSeenUpdateMillis: IProperty<Long>
}

class UpdateImpl (
        w: Worker,
        xx: Environment,
        val ctx: Context = xx().instance()
) : Update() {

    override val lastSeenUpdateMillis = newPersistedProperty(w, APrefsPersistence(ctx, "lastSeenUpdate"),
            { 0L })
}

fun newUpdateModule(ctx: Context): Kodein.Module {
    return Kodein.Module {
        bind<Update>() with singleton {
            UpdateImpl(w = with("gscore").instance(), xx = lazy)
        }
        bind<UpdateCoordinator>() with singleton {
            UpdateCoordinator(xx = lazy, downloader = AUpdateDownloader(ctx = instance()))
        }
        onReady {
            val s: Filters = instance()
            val t: Tunnel = instance()
            val ui: UiState = instance()
            val u: Update = instance()
            val repo: Repo = instance()

            // Check for update periodically
            t.tunnelState.doWhen { t.tunnelState(TunnelState.ACTIVE) }.then {
                // This "pokes" the cache and refreshes if needed
                repo.content.refresh()
            }

            // Display an info message when update is available
            repo.content.doOnUiWhenSet().then {
                if (isUpdate(ctx, repo.content().newestVersionCode)) {
                    u.lastSeenUpdateMillis.refresh(force = true)
                }
            }

            // Display notifications for updates
            u.lastSeenUpdateMillis.doOnUiWhenSet().then {
                val content = repo.content()
                val last = u.lastSeenUpdateMillis()
                val cooldown = 86400 * 1000L
                val env: Time = instance()
                val j: Journal = instance()

                if (isUpdate(ctx, content.newestVersionCode) && canShowNotification(last, env, cooldown)) {
                    notificationMain.show(UpdateNotification(content.newestVersionName))
                    u.lastSeenUpdateMillis %= env.now()
                }
            }


        }
    }
}

internal fun canShowNotification(last: Long, env: Time, cooldownMillis: Long): Boolean {
    return last + cooldownMillis < env.now()
}

fun isUpdate(ctx: Context, code: Int): Boolean {
    return code > BuildConfig.VERSION_CODE
}
