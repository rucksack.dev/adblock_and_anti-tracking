/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.content.Context
import org.blokada.BuildConfig

enum class Product {
    FULL, GOOGLE;

    companion object {
        fun current(ctx: Context): Product {
            return when(ctx.packageName) {
                "org.blokada",
                "org.blokada.origin.alarm",
                "org.blokada.alarm" -> Product.FULL
                "org.blokada.alarm.dnschanger" -> Product.GOOGLE
                else -> Product.FULL
            }
        }
    }
}

enum class ProductType {
    DEBUG, RELEASE, OFFICIAL, BETA;

    companion object {
        fun current(): ProductType {
            return when(BuildConfig.BUILD_TYPE.toLowerCase()) {
                "debug" -> DEBUG
                "official" -> OFFICIAL
                "beta" -> BETA
                else -> RELEASE
            }
        }

        fun isPublic(): Boolean {
            return current() in listOf(RELEASE, OFFICIAL)
        }
    }
}
