/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import blocka.CurrentAccount
import com.github.salomonbrys.kodein.instance
import com.github.salomonbrys.kodein.with
import gs.environment.Worker
import gs.property.newProperty
import ui.AbstractWebActivity
import java.net.URL

var shouldRefreshAccount = false

fun getActivateUrl(): URL {
    val cfg = get(CurrentAccount::class.java)
    return URL("https://app.blokada.org/activate/${cfg.id}")
}

fun getSupportUrl(): URL {
    val cfg = get(CurrentAccount::class.java)
    return URL("https://app.blokada.org/support?account-id=${cfg.id}")
}

class SubscriptionActivity : AbstractWebActivity() {

    private val ktx = ktx("SubsbcriptionActivity")
    private val w: Worker by lazy { ktx.di().with("gscore").instance<Worker>() }

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        targetUrl = newProperty(w, { getActivateUrl() })
        shouldRefreshAccount = true

        super.onCreate(savedInstanceState)
    }

}
