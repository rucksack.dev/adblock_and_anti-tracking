/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.app.Activity
import android.util.Base64
import core.bits.EnterDomainVB
import core.bits.EnterFileNameVB
import core.bits.EnterNameVB
import org.blokada.R
import tunnel.Filter
import tunnel.FilterSourceDescriptor
import tunnel.showSnack
import java.io.File


class StepActivity : Activity() {

    companion object {
        const val EXTRA_WHITELIST = "whitelist"
    }

    private val stepView by lazy { findViewById<VBStepView>(R.id.view) }
    private val ktx = ktx("StepActivity")
    private var whitelist: Boolean = false

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vbstepview)

        whitelist = intent.getBooleanExtra(EXTRA_WHITELIST, false)

        val nameVB = EnterNameVB(ktx, accepted = {
            name = it
            saveNewFilter()
        })

        stepView.pages = listOf(
                EnterDomainVB(ktx,
                        accepted = {
                            nameVB.inputForGeneratingName = if (it.size == 1) it.first().source else ""
                            sources = it
                            stepView.next()
                        },
                        fileImport = {
                            val path = File(getExternalPath(), "/filters/")
                            val files = path.listFiles()
                            if (files == null || files.isEmpty()) {
                                showSnack(R.string.slot_enter_domain_no_file)
                            } else {
                                stepView.pages = listOf(
                                        EnterFileNameVB(ktx, files) { file ->
                                            val f = Filter(
                                                    id(file.replace('/', '.'), whitelist = false),
                                                    source = FilterSourceDescriptor("file", file),
                                                    active = true,
                                                    whitelist = whitelist
                                            )
                                            entrypoint.onSaveFilter(f)
                                            finish()
                                        },
                                        nameVB)
                            }
                        }),
                nameVB
        )
    }

    override fun onBackPressed() {
//        if (!dashboardView.handleBackPressed()) super.onBackPressed()
        super.onBackPressed()
    }


    private var sources: List<FilterSourceDescriptor> = emptyList()
    private var name = ""

    private fun saveNewFilter() = when {
        sources.isEmpty() || name.isBlank() -> Unit
        else -> {
            sources.map {
                val name = if (sources.size == 1) this.name else this.name + " (${it.source})"
                Filter(
                        id = sourceToId(it),
                        source = it,
                        active = true,
                        whitelist = whitelist,
                        customName = name
                )
            }.apply {
                entrypoint.onSaveFilter(this)
                Unit
            }
            finish()
        }
    }

    private fun sourceToId(source: FilterSourceDescriptor): String {
        return "custom-filter:" + Base64.encodeToString(source.source.toByteArray(), Base64.NO_WRAP)
    }

}
