/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gs.environment

import com.github.salomonbrys.kodein.LazyKodein
import nl.komponents.kovenant.buildDispatcher

typealias Environment = LazyKodein

interface Time {
    fun now(): Long
}

class SystemTime : gs.environment.Time {
    override fun now(): Long {
        return System.currentTimeMillis()
    }
}

typealias Worker = nl.komponents.kovenant.Context

fun newSingleThreadedWorker(j: gs.environment.Journal, prefix: String): gs.environment.Worker {
    return nl.komponents.kovenant.Kovenant.createContext {
        callbackContext.dispatcher = buildDispatcher {
            name = "$prefix-callback"
            concurrentTasks = 1
            errorHandler = { j.log(it) }
            exceptionHandler = { j.log(it) }
        }
        workerContext.dispatcher = buildDispatcher {
            name = "$prefix-worker"
            concurrentTasks = 1
            errorHandler = { j.log(it) }
            exceptionHandler = { j.log(it) }
        }
    }
}

fun newConcurrentWorker(j: gs.environment.Journal?, prefix: String, tasks: Int): gs.environment.Worker {
    return nl.komponents.kovenant.Kovenant.createContext {
        callbackContext.dispatcher = buildDispatcher {
            name = "$prefix-callbackX"
            concurrentTasks = 1
            errorHandler = { j?.log(it) }
            exceptionHandler = { j?.log(it) }
        }
        workerContext.dispatcher = buildDispatcher {
            name = "$prefix-workerX"
            concurrentTasks = tasks
            errorHandler = { j?.log(it) }
            exceptionHandler = { j?.log(it) }
        }
    }
}

