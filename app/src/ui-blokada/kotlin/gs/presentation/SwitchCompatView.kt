/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gs.presentation

class SwitchCompatView(
        private val ctx: android.content.Context,
        attributeSet: android.util.AttributeSet?
) : androidx.appcompat.widget.SwitchCompat(ctx, attributeSet) {

    private var isInSetChecked = false

    override fun setChecked(checked: Boolean) {
        isInSetChecked = true
        super.setChecked(checked)
        isInSetChecked = false
    }

    override fun isShown(): Boolean {
        if (isInSetChecked) {
            return visibility == android.view.View.VISIBLE
        }
        return super.isShown()
    }
}
