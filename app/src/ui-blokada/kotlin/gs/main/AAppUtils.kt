/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package gs.main

import android.annotation.TargetApi
import android.app.AlarmManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.github.salomonbrys.kodein.instance
import gs.environment.Journal
import gs.environment.inject


internal fun registerUncaughtExceptionHandler(ctx: android.content.Context) {
    val defaultHandler = Thread.getDefaultUncaughtExceptionHandler()
    Thread.setDefaultUncaughtExceptionHandler { thread, ex ->
        gs.main.restartApplicationThroughService(ctx, 2000)
//        defaultHandler.uncaughtException(thread, ex)
        System.exit(2)
    }
}

private fun restartApplicationThroughService(ctx: android.content.Context, delayMillis: Int) {
    val alarm: android.app.AlarmManager = ctx.getSystemService(Context.ALARM_SERVICE) as AlarmManager

    val restartIntent = android.content.Intent(ctx, RestartService::class.java)
    val intent = android.app.PendingIntent.getService(ctx, 0, restartIntent, 0)
    alarm.set(android.app.AlarmManager.RTC, System.currentTimeMillis() + delayMillis, intent)
}

@TargetApi(24)
internal fun getPreferredLocales(): List<java.util.Locale> {
    val cfg = android.content.res.Resources.getSystem().configuration
    return try {
        // Android, a custom list type that is not an iterable. Just wow.
        val locales = cfg.locales
        (0..locales.size() - 1).map { locales.get(it) }
    } catch (t: Throwable) { listOf(cfg.locale) }
}

class RestartService : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val j: Journal = inject().instance()
        j.log("RestartService start command")
        return Service.START_STICKY;
    }

}
