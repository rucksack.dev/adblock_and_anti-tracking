/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package filter

interface IHostlineProcessor {
    fun process(line: String): String?
}

class DefaultHostlineProcessor: IHostlineProcessor {
    override fun process(line: String): String? {
        var l = line
        if (l.startsWith("#")) return null
        if (l.startsWith("<")) return null
        l = l.replaceFirst("0.0.0.0 ", "")
        l = l.replaceFirst("127.0.0.1 ", "")
        l = l.replaceFirst("127.0.0.1	", "")
        l = l.trim()
        if (l.isEmpty()) return null
        if (!hostnameRegex.containsMatchIn(l)) return null
        return l
    }
}

