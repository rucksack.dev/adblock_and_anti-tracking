/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package notification

import android.app.IntentService
import android.content.Intent
import android.os.Handler
import core.entrypoint
import core.id
import org.blokada.R
import tunnel.Filter
import tunnel.FilterSourceDescriptor

class ANotificationsWhitelistService : IntentService("notificationsWhitelist") {

    private var mHandler: Handler = Handler()

    override fun onHandleIntent(intent: Intent) {
        val host = intent.getStringExtra("host") ?: return

        val f = Filter(
                id(host, whitelist = true),
                source = FilterSourceDescriptor("single", host),
                active = true,
                whitelist = true
        )

        entrypoint.onSaveFilter(f)

        mHandler.post(DisplayToastRunnable(this, getString(R.string.notification_blocked_whitelist_applied)))
        notificationMain.cancel(FilteredNotification(""))
    }

}
