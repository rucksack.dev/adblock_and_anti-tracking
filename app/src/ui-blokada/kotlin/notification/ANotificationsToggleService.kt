/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package notification

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.widget.Toast
import com.github.salomonbrys.kodein.instance
import core.Tunnel
import core.entrypoint
import gs.environment.inject
import org.blokada.R


class ANotificationsToggleService : IntentService("notificationsToggle") {
    private var mHandler: Handler = Handler()

    override fun onHandleIntent(intent: Intent) {
         val newState: Boolean = intent.getBooleanExtra("new_state", true)
        when (intent.getSerializableExtra("setting") as NotificationsToggleSeviceSettings) {
            NotificationsToggleSeviceSettings.GENERAL -> {
                val t: Tunnel = this.inject().instance()
                t.enabled %= newState
                if(newState){
                    mHandler.post(DisplayToastRunnable(this, this.resources.getString(R.string.notification_keepalive_activating)))
                }else{
                    mHandler.post(DisplayToastRunnable(this, this.resources.getString(R.string.notification_keepalive_deactivating)))
                }
            }
            NotificationsToggleSeviceSettings.ADBLOCKING -> {
                entrypoint.onSwitchAdblocking(newState)

            }
            NotificationsToggleSeviceSettings.DNS -> {
                entrypoint.onSwitchDnsEnabled(newState)
            }
            NotificationsToggleSeviceSettings.VPN -> {
                entrypoint.onVpnSwitched(newState)

            }
        }
    }

}

class DisplayToastRunnable(private val mContext: Context, private var mText: String) : Runnable {
    override fun run() {
        Toast.makeText(mContext, mText, Toast.LENGTH_SHORT).show()
    }
}

enum class NotificationsToggleSeviceSettings {
    GENERAL, ADBLOCKING, DNS, VPN
}